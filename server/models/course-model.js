import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import async from 'async';
import {DataObject} from "@themost/data/data-object";
import {HttpServerError} from "@themost/common/errors";

/**
 * @class
 * @param {*} obj
 * @augments DataObject
 */
class Course extends DataObject {
    constructor() {
        super();
    }

    passed(student, callback) {
        const self = this, context = self.context;
        try {
            if (_.isNil(student)) {
                return callback(null);
            }
            const studentCourses = context.model('StudentCourse'), courses = context.model('Course');
            studentCourses.where('student').equal(student).and('course').equal(self.id).and('isPassed').equal(1).silent().count(function(err,count) {
                if (err) { return callback(err);}
                if (count===1) {
                    return callback(null, true);
                }
                courses.where('id').equal(self.id).and('replacedCourse').notEqual(null).select('replacedCourse').silent().flatten().first(function(err, result) {
                    if (err) { return callback(err);}
                    if (_.isNil(result)) {
                        return callback(null, false);
                    }
                    const replacedCourse = result['replacedCourse'], arr = [];
                    if (typeof replacedCourse === 'string' && replacedCourse.length>0) {
                        arr.push.call(arr, replacedCourse.split(','));
                    }
                    if (arr.length===0) {
                        return callback(null, false);
                    }
                    for (let i = 0; i < arr.length; i++) {
                       if (_.isNumber(arr[i])===false) {
                           TraceUtils.error(`Error converting replacedIds <${arr[i]}>`);
                           return callback(null, false);
                       }
                    }

                    return courses.where('id').in(arr).select('id').silent().getAllTypedItems().then((result) => {
                        let res = false;
                        return (async () => {
                            for (let i = 0; i < result.length; i++) {
                                await new Promise((resolve, reject) => {
                                    /**
                                     * @type {Course}
                                     */
                                    let course = result[i];
                                    course.passed(student,function(err, value) {
                                        if (value) {
                                            res = value;
                                            return reject(Object.assign(new Error('Break'), {
                                                code: 'EBREAK'
                                            }));
                                        }
                                        else {
                                            return resolve();
                                        }
                                    });
                                });
                            }
                        })().then(()=> {
                            return callback(null, res);
                        }).catch(err=> {
                            if (err && err.code !== 'ABRK') {
                                return callback(err);
                            }
                            return callback(null, res);
                        });

                    }).catch(err => {
                        return callback(err);
                    });
                });
            });
        }
        catch (err) {
            callback(err);
        }
    }

    replacedPassed(student, callback) {
        const self = this, context = self.context;
        try {
            if (_.isNil(student)) {
                return callback(null);
            }
            //var studentCourses = context.model('StudentCourse'),
            const courses = context.model('Course');
            courses.where('id').equal(self.id).and('replacedCourse').notEqual(null).select(['replacedCourse']).silent().flatten().first(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(null, false);
                }
                const replacedCourse = result['replacedCourse'], arr = [];
                if (typeof replacedCourse === 'string' && replacedCourse.length > 0) {
                    arr.push.call(arr, replacedCourse.split(','));
                }
                if (arr.length === 0) {
                    return callback(null, false);
                }
                for (let i = 0; i < arr.length; i++) {
                    if (_.isNumber(arr[i]) === false) {
                        TraceUtils.error(`Error converting replacedIds <${arr[i]}>`);
                        return callback(null, false);
                    }
                }
                courses.where('id').in(arr).select('id').flatten().silent().all(function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    if (result.length === 0) {
                        return callback(null, false);
                    }
                    let res = false, course;
                    async.eachSeries(result, function (item, cb) {
                        course = courses.convert(item);
                        course.passed(student, function (err, value) {
                            if (value) {
                                res = value;
                                const er = new Error('Break');
                                er.code = 'ABRK';
                                cb(er);
                            }
                            else {
                                cb();
                            }
                        });
                    }, function (err) {
                        if (err && err.code !== 'ABRK') {
                            return callback(err);
                        }
                        callback(null, res);
                    });
                });
            });
        }
        catch (e) {
            callback(e);
        }
    }

    lastGrades(callback) {
        const self = this, context = self.context;
        //get course
        context.model('Course').where('id').equal(self.id).select('id','department').flatten().first(function(err, course) {
            if (err) {
                return callback(err);
            }
            else if (_.isNil(course)) {
                return callback(null, self.json([]));
            }
            else {
                //get course department and check if showing grades is allowed.
                context.model('LocalDepartment').where('id').equal(course.department).select('id','webShowGrades','webResultsYear','currentYear').first(function(err, department) {
                    if (err) {
                        return callback(err);
                    }
                    if (_.isNil(department)) {
                        return callback(null, self.json([]));
                    }
                    if (LangUtils.parseInt(department.webShowGrades)===0)
                    {
                        //department does not allow showing grades
                        return callback(null, {error: 'Course department does not allow showing grades'});
                    }

                    const resultYear=department.webResultsYear || department.currentYear;
                    //get request params
                    const params = Object.assign({}, context.params);
                    delete params.id;
                    //get student grades for specific year in silent mode
                    context.model('StudentCourse').filter(params, function (err, q) {
                        q.where('course').equal(self.id).and('gradeYear').equal(resultYear);
                        q.select('student/studentIdentifier as studentIdentifier', 'gradePeriodDescription', 'formattedGrade', 'gradeYear');
                        const $top = LangUtils.parseInt(context.params.$top);
                        if ($top > 0) {
                            q.take($top).silent().list(function (err, result) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return callback(new HttpServerError());
                                }
                                callback(null, {grades: result.records, total: result.total});
                            });
                        }
                        else {
                            q.silent().all(function (err, result) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return callback(new HttpServerError());
                                }
                                callback(null, {grades: result});
                            });
                        }
                    });
                });
            }
        });
    }
}

module.exports = Course;
