import {EdmMapping,EdmType} from '@themost/data/odata';
import EnableAttachmentModel from "./enable-attachment-model";

/**
 * @class
 
 * @property {string} subject
 * @property {string} body
 * @property {Account|any} sender
 * @property {Account|any} recipient
 * @property {string} cc
 * @property {string} bcc
 * @property {string} category
 * @property {Date} dateReceived
 * @property {number} owner
 * @property {Action|any} action
 * @property {string} identifier
 * @property {Array<Attachment|any>} attachments
 * @property {number} id
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {EnableAttachmentModel}
 */
@EdmMapping.entityType('Message')
class Message extends EnableAttachmentModel {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Message;