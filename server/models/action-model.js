import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {number} result
 * @property {ActionStatusType|any} actionStatus
 * @property {EntryPoint|any} target
 * @property {User|any} owner
 * @property {User|any} agent
 * @property {Date} startTime
 * @property {Date} endTime
 * @property {Array<Account|any>} participants
 * @property {number} object
 * @property {number} error
 * @property {string} code
 * @property {Array<Attachment|any>} attachments
 * @property {Array<Account|any>} assignees
 * @property {Array<Account|any>} followers
 * @property {number} id
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('Action')
class Action extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Action;