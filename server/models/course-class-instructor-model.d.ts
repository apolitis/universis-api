import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import CourseClass = require('./course-class-model');
import Instructor = require('./instructor-model');

/**
 * @class
 */
declare class CourseClassInstructor extends DataObject {

     
     /**
      * @description Id
      */
     public id: string; 
     
     /**
      * @description Course Class - Διδασκαλία μαθήματος
      */
     public courseClass: CourseClass|any; 
     
     /**
      * @description Ο διδάσκων της τάξης
      */
     public instructor: Instructor|any; 
     
     /**
      * @description dateModified
      */
     public dateModified: Date; 

}

export = CourseClassInstructor;