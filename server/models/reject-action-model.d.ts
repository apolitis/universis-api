import {EdmMapping,EdmType} from '@themost/data/odata';
import AllocateAction = require('./allocate-action-model');

/**
 * @class
 */
declare class RejectAction extends AllocateAction {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = RejectAction;