import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import CourseClass = require('./course-class-model');

/**
 * @class
 */
declare class CourseClassSection extends DataObject {

     
     /**
      * @description Κωδικός τμήματος τάξης μαθήματος
      */
     public id: string; 
     
     /**
      * @description Η τάξη του μαθήματος που αναφέρεται το τμήμα
      */
     public courseClass: CourseClass|any; 
     
     /**
      * @description Ο αριθμός του τμήματος
      */
     public section?: number; 
     
     /**
      * @description Αύξων αριθμός τμήματος
      */
     public sectionIndex?: number; 
     
     /**
      * @description Ο τίτλος του τμήματος της τάξης
      */
     public name?: string; 
     
     /**
      * @description Μέγιστος αριθμός φοιτητών που δέχεται το τμήμα τάξης.
      */
     public maxNumberOfStudents?: number; 
     
     /**
      * @description Οι διδάσκοντες του τμήματος τάξης.
      */
     public instructors?: string; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Αν είναι ενεργό το τμήμα
      */
     public active?: boolean; 
     
     /**
      * @description Κατηγορία τμήματος τάξης
      */
     public sectionCategory?: number; 
     
     /**
      * @description Συνθήκη τμήματος τάξης
      */
     public sectionFilter?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 

}

export = CourseClassSection;