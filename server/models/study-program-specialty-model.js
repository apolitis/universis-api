import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {string} id
 * @property {StudyProgram|any} studyProgram
 * @property {number} specialty
 * @property {string} name
 * @property {string} abbreviation
 */
@EdmMapping.entityType('StudyProgramSpecialty')
class StudyProgramSpecialty extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StudyProgramSpecialty;
