/**
 * @license
 * Universis Project Version 1.0
 * Copyright (c) 2018, Universis Project All rights reserved
 *
 * Use of this source code is governed by an LGPL 3.0 license that can be
 * found in the LICENSE file at https://universis.io/license
 */
import express from 'express';
import {HttpNotFoundError} from "@themost/common";
import multer from "multer";
import {interactiveStudent, validateStudent} from '../middlewares';
let router = express.Router();
// get user storage
const upload = multer({ dest: 'content/user/' });

router.post('/:id/messages/send', upload.single('attachment'),async function sendMessage (req, res, next) {
    try {
        // get student
        let student = await req.context.model('Student').where('id').equal(req.params.id).select('id').getTypedItem();
        if (typeof student === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError('Student cannot be found'));
        }
        let result = await new Promise((resolve, reject) => {
            let finalResult;
            return req.context.db.executeInTransaction((cb) => {
                let message=Object.assign({ }, req.body, {
                    student: student.id
                });
                if (message.action)
                {
                    message.action=parseInt(message.action);
                }
                return req.context.model('StudentMessage').save(message).then(message => {
                    // convert message to studentMessage
                    finalResult = req.context.model('StudentMessage').convert(message);
                    if (req.file) {
                        // add attachment to message
                        return finalResult.addAttachment(req.file).then(result => {
                            finalResult.attachments=[];
                            finalResult.attachments.push(result);
                            return cb();
                        });
                    }
                    return cb();
                }).catch(err => {
                    return cb(err);
                });
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        });
        return res.json(result);
    }
    catch (err) {
        return next(err);
    }
});

router.get('/me/currentRegistration/effectiveStatus', interactiveStudent(),
    validateStudent(),
    async function getRegistrationEffectiveStatus(req, res, next) {
    try {
        // return current registration status
        const status = await req.student.getCurrentRegistrationStatus();
        // return status
        return res.json(status);
    }
    catch (err) {
        return next(err);
    }
});

module.exports = router;
