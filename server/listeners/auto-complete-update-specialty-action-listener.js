import ActionStatusType from '../models/action-status-type-model';
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const target = event.model.convert(event.target);
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.ActiveActionStatus) {
        // exit
        return;
    }
    // complete action status
    Object.assign(target, {
        actionStatus: {
            alternateName: ActionStatusType.CompletedActionStatus
        }
    });
    // save target
    await event.model.silent().save(target);
    // complete action status
    Object.assign(event.target, target);
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
