import ActionStatusType from '../models/action-status-type-model';
/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }
    const target = event.model.convert(event.target);
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
        // exit
        return;
    }
    // get previous action status
    const previousActionStatus = event.previous && event.previous.actionStatus;
    if (previousActionStatus === null) {
        throw new Error('Previous status cannot be empty at this context.');
    }
    // validate previous status
    if (previousActionStatus.alternateName !== 'ActiveActionStatus') {
        // throw error for invalid previous action status
        throw new Error('Invalid action state. An action cannot be completed due to its previous state.');
    }
    // execute action by assigning student and specialty to an instance of StudentSpecialty class
    // get student
    const student = await target.property('object').silent().getItem();
    // get requested specialty
    const specialty = await target.property('specialty').silent().getItem();
    /**
     * get student specialty
     * @type {*}
     */
    const studentSpecialty = event.model.context.model('StudentSpecialty').convert({
        student: student,
        specialty: specialty
    });
    // do save
    await studentSpecialty.save();
}


export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
